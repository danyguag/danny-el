;;; mu4e Configuration

(use-package mu4e
  :ensure nil
  :load-path "/usr/local/share/emacs/site-lisp/mu4e"
  :bind ("C-c m" . mu4e)
  :custom ((auth-source "~/authinfo")
	   (mu4e-change-filenames-when-moving t)
	   (mu4e-update-interval (* 10 60))
	   (mu4e-get-mail-command "mbsync -a")
	   (mu4e-maildir "~/MailDir")  
	   (mu4e-drafts-folder "/danyguag2.drafts")
	   (mu4e-sent-folder "/danyguag2.sent")
	   (mu4e-refile-folder "/danyguag2.All Mail")
	   (mu4e-trash-folder "/danyguag2.Trash")
	   (mu4e-maildir-shortcuts
	    '(("/danyguag2"        . ?i)
	      ("/danyguag2.sent"   . ?s)
	      ("/danyguag2.trash"  . ?t)
	      ("/danyguag2.drafts" . ?d)
	      ("/danyguag2.all"    . ?a)))
	   (user-mail-address "danyguag2@gmail.com".)
	   (user-full-name "Daniel Guagliardo")  
	   (smtpmail-smtp-server "smtp.gmail.com")
	   (smtpmail-smtp-service 587)))
