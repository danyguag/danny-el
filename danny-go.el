(use-package go-mode
  :ensure t
  :mode "\\.go$"
  :interpreter "go"
  :config
  (setq css-indent-offset 8))

(use-package go-eldoc
  :ensure t
  :config
  (add-hook 'go-mode-hook 'go-eldoc-setup))

(use-package company-go
  :ensure t
  :config
  (setq company-go-show-annotation t)
  (push 'company-go company-backends)
  (global-company-mode))
