;;; danny-base -- Base emacs configuration
;;; Commentary:
;;; This package provides the very basic and needed improvements and packages
;;; to Emacs itself.  This allows for simple Emacs use.

;; package.el Initialization
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

; Initialize package.el and refresh the sources in case of package installation
(package-initialize)
(package-refresh-contents)
;;

;; Install use-package if its not already installed then require it
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
;;

;; Text size
(set-face-attribute 'default nil
		    :family "Noto Sans Mono"
		    :height 125)
;;

;; Basic Emacs Configuration
(use-package vertico
  :ensure t
  :init
  (vertico-mode))

(use-package consult
  :ensure t
  :bind (
	 ("C-s" . consult-line)
	 ("C-x b" . consult-buffer)
	 ("C-c l" . find-file)
	 ("C-c u" . switch-to-buffer-other-window)
	 ("M-f" . consult-imenu)))

(use-package sqlite3
  :ensure t)

(use-package which-key
  :ensure t
  :init (which-key-mode)
  :custom (which-key-idle-delay 0.3))

(use-package orderless
  :ensure t
  :custom (completion-styles '(orderless)))

(use-package marginalia
  :after vertico
  :ensure t
  :init
  (marginalia-mode))

(use-package flycheck
  :ensure t
  :config
  (global-flycheck-mode))

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode))

(use-package helpful
  :ensure t
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind (
	 ([remap describe-function] . helpful-function)
	 ([remap describe-command] . helpful-command)
	 ([remap describe-variable] . helpful-variable)
	 ([remap describe-key] . helpful-key)))

(use-package savehist
  :init
  (savehist-mode))

(use-package eww
  :ensure t
  :custom (eww-search-prefix "https://lite.duckduckgo.com/lite?kd=-1&kp=-1&q="))

(use-package auto-package-update
   :ensure t
   :config
   (setq auto-package-update-delete-old-versions t
         auto-package-update-interval 4)
   (auto-package-update-maybe))
;;

;; General Visual Package Configuration

; Remove all of the mouse oriented ui stuff
(defun danny/post-window-load ()
  (interactive)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (toggle-frame-maximized))

(add-hook 'window-setup-hook 'danny/post-window-load t)

(setq c-basic-offset 4)
(setq c-default-style "bsd")
; Turn on line numbers
(column-number-mode)
(global-display-line-numbers-mode t)

; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;(use-package feebleline
;  :ensure t
;  :config (feebleline-mode))

(use-package doom-themes
  :ensure t
  :config (load-theme 'doom-dark+ t))
;;

;; Project Management Packages

(use-package magit
  :ensure t)

(use-package git-gutter
  :ensure t
  :config
  (global-git-gutter-mode))

(use-package forge
  :ensure t
  :after magit
  :custom (auth-source "~/authinfo"))

(use-package project
  :ensure t
  :bind ("C-c t" . project-compile))

(use-package embark
  :ensure t
  :bind ("C-," . embark-act))

(use-package embark-consult
  :ensure t)
;;

;; Misc Configuration
(setq dabbrev-case-replace t)
(setq dabbrev-case-fold-search t)
(setq dabbrev-upcase-means-case-search t)
;;;

;; Custom Keybindings
(global-set-key (kbd "C-j") 'find-file-other-window)
(global-set-key (kbd "C-i") 'delete-other-windows)
(global-set-key (kbd "C-k") 'split-window-right)
(global-set-key (kbd "M-u") 'undo)
(global-set-key (kbd "C-f") 'yank)
(global-set-key (kbd "C-e") 'query-replace)
(global-set-key (kbd "C-q") 'kill-ring-save)
(global-set-key (kbd "M-s") 'save-buffer)
(define-key global-map "\t" 'dabbrev-expand)

;; Disable lockfiles and backup files
(setq create-lockfiles nil)
(setq make-backup-files nil)

(use-package dockerfile-mode
  :ensure t)
