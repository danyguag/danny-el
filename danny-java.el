;;;; Java Development Environment

(use-package eclim
  :ensure t
  :custom
  (eclim-executable "/home/danyguag/.eclipse/155965261_linux_gtk_x86_64/plugins/org.eclim_2.18.0/bin/eclim")
  (eclimd-default-workspace "/home/danyguag/projects/")
  :config

  (setq help-at-pt-display-when-idle t)
  (setq help-at-pt-timer-delay 0.1)
  (help-at-pt-set-timer)

  (add-hook 'java-mode-hook 'eclim-mode)
  )

(use-package company-emacs-eclim
  :ensure t
  :config
  (company-emacs-eclim-setup)
  )

(defun dg/start-eclimd ()
  (eclimd-start "~/projects/")
  )

;;;;
