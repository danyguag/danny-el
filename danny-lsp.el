;;; danny-lsp -- LSP Setup
;;; Commentary: This packages sets up the needed packages for using LSP. Right now
;;; there is very lit

(use-package lsp-mode
  :after flycheck
  :ensure t
  :init
  (setq lsp-keymap-prefix "C-c s")
  :config
  (lsp-register-custom-settings
   '(("pyls.plugins.pyls_mypy.enabled" t t)
     ("pyls.plugins.pyls_mypy.live_mode" nil t)
     ("pyls.plugins.pyls_black.enabled" t t)
     ("pyls.plugins.pyls_isort.enabled" t t)
     ("pyls.plugins.flake8.enabled" t t)))
  (lsp-enable-which-key-integration t)
  (add-hook 'c-mode-hook 'lsp)
  (add-hook 'c++-mode-hook 'lsp)
  (add-hook 'python-mode-hook 'lsp)
  (lsp-register-client
    (make-lsp-client :new-connection (lsp-tramp-connection "pyls")
                     :major-modes '(python-mode)
                     :remote? t
                     :server-id 'pyls-remote)))

(use-package python-mode
  :ensure t
  :config
  (add-hook 'python-mode-hook 'company-mode))

(use-package lsp-ui
  :ensure t
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-position 'bottom))

(use-package company
  :ensure t
  :after lsp-mode
  :bind (:map company-active-map
	      ("<tab>" . company-complete-selection))
  (:map lsp-mode-map
	("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0)
  :config
  (add-hook 'lsp-mode 'company-mode)
  (global-company-mode))

(use-package company-box
  :ensure t
  :hook (company-mode . company-box-mode))

(use-package pytest
  :ensure t)

(use-package dap-mode
  :ensure t
  :custom
  (dap-auto-configure-features '(sessions locals controls tooltip))
  :bind (("C-c d i" . dap-step-in)
	 ("C-c d n" . dap-next))
  :config
  (require 'dap-python)
  ;; if you installed debugpy, you need to set this
  ;; https://github.com/emacs-lsp/dap-mode/issues/306
  (setq dap-python-debugger 'debugpy)
  (dap-register-debug-template "My App"
			       (list :type "python"
				     :args "--config=data/config.json"
				     :cwd "/home/danyguag/projects/vsell/"
				     :target-module (expand-file-name "~/projects/vsell")
				     :request "launch"
				     :name "VSell Debug Template")))

;;; danny-lsp.el ends here
