;;; Elfeed Configuration for Rss Feed

(use-package elfeed
  :ensure t
  :bind ("C-x w" . elfeed)
  :custom (
	   (elfeed-feeds
	    '("https://csdl-api.computer.org/api/rss/periodicals/mags/co/rss.xml"
	      "https://csdl-api.computer.org/api/rss/periodicals/letters/ca/rss.xml"
	      "https://csdl-api.computer.org/api/rss/periodicals/mags/so/rss.xml"))))
