;;; Rtags(for C++) Configuration File

(use-package rtags
  :ensure t
  :init (rtags-start-process-unless-running)
  :custom ((rtags-autostart-diagnostics t)
	   (rtags-completions-enabled t))
  :config
  (rtags-enable-standard-keybindings)
  (rtags-diagnostics)
  (define-key c-mode-base-map (kbd "M-.") 'rtags-find-symbol-at-point)
  (define-key c-mode-base-map (kbd "M-,") 'rtags-find-references-at-point)
  (define-key c-mode-base-map (kbd "M-?") 'rtags-display-summary)
  (define-key c-mode-base-map (kbd "M-'") 'rtags-rename-symbol))

(use-package flycheck
  :ensure t
  :config
  (add-hook 'c++-mode-hook (lambda () (setq flycheck-gcc-language-standard "c++17")))
  (add-hook 'c++-mode-hook (lambda () (setq flycheck-gcc-args "-DPLATFORM_LINUX")))
  (add-hook 'after-init-hook 'global-flycheck-mode))

(use-package flycheck-rtags
  :ensure t
  :config
  ;; ensure that we use only rtags checking
  ;; https://github.com/Andersbakken/rtags/wiki/Usage
  (defun setup-flycheck-rtags ()
    (flycheck-select-checker 'rtags)
    (setq-local flycheck-highlighting-mode nil) ;; RTags creates more accurate overlays.
    (setq-local flycheck-check-syntax-automatically nil)
    (rtags-set-periodic-reparse-timeout 2.0)  ;; Run flycheck 2 seconds after being idle.
    )

  (add-hook 'c-mode-hook #'setup-flycheck-rtags)
  (add-hook 'c++-mode-hook #'setup-flycheck-rtags))

(use-package company-rtags
  :ensure t
  :custom (rtags-completions-enabled t)
  :config (push 'company-rtags company-backends))

(defun danny/build-function ()
  (interactive)
  (compile "~/bin/limelight build"))

(use-package clang-format
  :ensure t
  :bind (("M-b" . clang-format-buffer)
	 ("C-<tab>" . clang-format-region)
	 ("M-m" . danny/build-function)))
