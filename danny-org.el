;;; Org Mode Setup

(defun danny/org-mode-setup ()
  (variable-pitch-mode 1)
  (visual-line-mode 1)
  (set-face-attribute 'org-table nil :inherit 'fixed-pitch))

(use-package org
  :hook ((org-mode . danny/org-mode-setup)
	 (org-mode . flyspell-mode)
	 (text-mode . flyspell-mode)
	 (markdown-mode . flyspell-mode))
  :bind ("C-c ." . 'org-time-stamp)
  :custom (
	   (org-ellipsis " ▾")
	   (org-agenda-start-with-log-mode t)
	   (org-log-done 'time)
	   (org-log-into-drawer t)
	   (org-agenda-include-diary t)
	   (org-agenda-files
	    '("~/OrgFiles/Personal.org"
	      "~/OrgFiles/Tasks.org"
	      "~/OrgFiles/Me.org"))
	   (org-refile-targets
	    '(("~/OrgFiles/Archives.org" :maxlevel . 1)
	      ("~/OrgFiles/school/SchoolTasks.org" :maxlevel . 1)
	      ("~/OrgFiles/Tasks.org" :maxlevel . 1)))
	   (org-babel-default-header-args:sh '((:results . "output")))
	   (org-babel-default-header-args:shell '((:results . "output")))
	   (org-babel-load-languages '((emacs-lisp . t)
				       (shell . t)
				       (C . t))))
  :config (advice-add 'org-refile :after 'org-save-all-org-buffers))

(use-package org-capture
  :custom (org-capture-templates
	   '(("t" "Technical Task for Agenda" entry
	      (file+headline "~/OrgFiles/Tasks.org" "Todo List")
	      "* TODO %? %a %^G\n")
	     ("r" "Technical Random Task for Agenda" entry
	      (file+headline "~/OrgFiles/Tasks.org" "Todo List")
	      "* TODO %? %^G\nDEADLINE: %^t\n")
	     ("s" "School Assignment for Agenda" entry
	      (file+headline "~/OrgFiles/school/SchoolTasks.org" "School Tasks")
	      "* TODO %? %^G\nDEADLINE: %^t\n")
	     ("p" "Non-Technical Todo List" entry
	      (file+headline "~/OrgFiles/Personal.org" "Non-technical Todo List")
	      "* TODO %? %^G\nDEADLINE: %^t\n")
	     ("a" "Schedule an Appointment" entry
	      (file+headline "~/OrgFiles/Personal.org" "Appointments")
	      "* TODO %? %^G\nSCHEDULED: %^t\n"))))

; Check here for information on configuring it https://github.com/Malabarba/org-agenda-property
(use-package org-agenda-property
  :ensure t
  :custom (org-agenda-property-list '("DESCRIPTION")))

(use-package org-journal
  :ensure t
  :custom (org-journal-dir "~/OrgFiles/journal/"))

(use-package org-timeline
  :ensure t
  :config
  (add-hook 'org-agenda-finalize-hook 'org-timeline-insert-timeline :append)
  (message "Why is this not running"))

;;;
