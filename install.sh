echo "Installing Emacs Configuration Files."
rm -rf $HOME/.emacs.d
mkdir $HOME/.emacs.d

if [ -z "$DANNYEL_HOME" ]
then
      DANNYEL_HOME=$(pwd)
fi

# Copy the init file and link the rest
cp $DANNYEL_HOME/init.el $HOME/.emacs.d/

ln $DANNYEL_HOME/danny-base.el $HOME/.emacs.d/danny-base.el
ln $DANNYEL_HOME/danny-mail.el $HOME/.emacs.d/danny-mail.el
ln $DANNYEL_HOME/danny-org.el $HOME/.emacs.d/danny-org.el
ln $DANNYEL_HOME/danny-pdf.el $HOME/.emacs.d/danny-pdf.el
ln $DANNYEL_HOME/danny-rtags.el $HOME/.emacs.d/danny-rtags.el
ln $DANNYEL_HOME/danny-rust.el $HOME/.emacs.d/danny-rust.el
ln $DANNYEL_HOME/danny-go.el $HOME/.emacs.d/danny-go.el
ln $DANNYEL_HOME/danny-java.el $HOME/.emacs.d/danny-java.el
ln $DANNYEL_HOME/danny-elf.el $HOME/.emacs.d/danny-elf.el
ln $DANNYEL_HOME/danny-lsp.el $HOME/.emacs.d/danny-lsp.el

echo "Completed."
