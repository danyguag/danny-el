; Start the Emacs Daemon
(server-start)

; Load the base packages 
(load "~/.emacs.d/danny-base.el")

; Other packages should be loaded here uncomment to enable
;(load "~/.emacs.d/danny-mail.el")
(load "~/.emacs.d/danny-org.el")
;(load "~/.emacs.d/danny-pdf.el")
;(load "~/.emacs.d/danny-rtags.el")
;(load "~/.emacs.d/danny-rust.el")
;(load "~/.emacs.d/danny-elf.el")
(load "~/.emacs.d/danny-lsp.el")
